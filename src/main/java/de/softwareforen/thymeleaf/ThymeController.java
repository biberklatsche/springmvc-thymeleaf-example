package de.softwareforen.thymeleaf;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/Test")
public class ThymeController {

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView index() {
		List<Message> messages = new ArrayList<Message>(3);
		messages.add(new Message(1, "Hallo", "Hallo Thymeleaf"));
		messages.add(new Message(2, "Hallo1", "Hallo Thymeleaf1"));
		messages.add(new Message(3, "Hallo2", "Hallo Thymeleaf2"));

		ModelAndView mv = new ModelAndView("index");

		mv.addObject("messages", messages);

		return mv; // name of the view to render the model
	}
}
